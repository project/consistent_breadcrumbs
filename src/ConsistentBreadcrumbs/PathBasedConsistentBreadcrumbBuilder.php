<?php

namespace Drupal\consistent_breadcrumbs\ConsistentBreadcrumbs;

use Drupal\consistent_breadcrumbs\BreadcrumbItem;
use Drupal\consistent_breadcrumbs\ConsistentBreadcrumbBuilderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

class PathBasedConsistentBreadcrumbBuilder implements ConsistentBreadcrumbBuilderInterface {

  protected ConfigFactoryInterface $configFactory;

  protected string $frontPagePath;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  public function getBreadcrumbItem(RouteMatchInterface $routeMatch, bool &$isFinalItem = NULL): BreadcrumbItem {
    $breadcrumbItem = BreadcrumbItem::create();
    $breadcrumbItem->addCacheContexts(['url.path.parent', 'url.path.is_front']);

    $url = Url::fromRouteMatch($routeMatch);
    $isFrontPage = ($url->getRouteName() && '/' . $url->getInternalPath() === $this->getFrontPagePath());

    if ($isFrontPage) {
      return $breadcrumbItem;
    }

    $path = Url::fromRouteMatch($routeMatch)->getInternalPath();
    $pathElements = explode('/', $path);
    array_pop($pathElements);
    $parentPath = implode('/', $pathElements);
    $parentUrl = Url::fromUri("internal:/$parentPath");
    $parentLink = Link::fromTextAndUrl('', $parentUrl);
    return $breadcrumbItem->setLink($parentLink);
  }

  private function getFrontPagePath() {
    if (!isset($this->frontPagePath)) {
      $this->frontPagePath = $this->configFactory->get('system.site')->get('page.front');
    }
    return $this->frontPagePath;
  }

}
