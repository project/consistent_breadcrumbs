<?php

namespace Drupal\consistent_breadcrumbs\ConsistentBreadcrumbs;

use Drupal\consistent_breadcrumbs\BreadcrumbItem;
use Drupal\consistent_breadcrumbs\ConsistentBreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

class TrivialConsistentBreadcrumbBuilder implements ConsistentBreadcrumbBuilderInterface {

  public function getBreadcrumbItem(RouteMatchInterface $routeMatch, bool &$isFinalItem = NULL): BreadcrumbItem {
    $isFinalItem = TRUE;
    return BreadcrumbItem::create(Link::fromTextAndUrl('', Url::fromUri('internal:/')))
      // Technically, cache context is empty. But caller throws otherwise.
      ->addCacheContexts(['route']);
  }

}
