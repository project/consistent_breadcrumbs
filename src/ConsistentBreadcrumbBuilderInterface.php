<?php

declare(strict_types=1);

namespace Drupal\consistent_breadcrumbs;

use Drupal\Core\Routing\RouteMatchInterface;

interface ConsistentBreadcrumbBuilderInterface {

  /**
   * Get next breadcrumb item.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match to get next breadcrumb for.
   * @param bool|null $isFinalItem
   *   Last slice info, returned by reference. You can (and usually) want to
   *   ignore this, in which case the manager checks if the last link of the
   *   returned breadcrumb slice is '<front>'. If you want a different behavior,
   *   set this accordingly.
   *
   * @return \Drupal\consistent_breadcrumbs\BreadcrumbItem
   *   The ConsistentBreadcrumbItem carries the next breadcrumb link info:
   *   - On match, return the next link, with cacheability.
   *   - On non-match, return no link, with cacheability.
   *   In any case, care to add cacheability! Even if no match, any following
   *   builder's breadcrumbs depend on your breadcrumbs not matching.
   *   Most common cacheability cases are:
   *   - 'route.name' context, if return value is only determined by route name.
   *   - 'route' context, if return value is determined by route name and
   *     parameters.
   *   - 'query.args:xxx' context, if return value is determined by query arg.
   *   You do not need to check link access, as this is done by the manager, and
   *   any item with a non-access link is discarded.
   *   You can (and usually want) to set the link text to '', in which case it
   *   is replaced by the default route title.
   */
  public function getBreadcrumbItem(RouteMatchInterface $routeMatch, bool &$isFinalItem = NULL): BreadcrumbItem;

}
