<?php

declare(strict_types=1);

namespace Drupal\consistent_breadcrumbs;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;

interface ConsistentBreadcrumbManagerInterface extends BreadcrumbBuilderInterface {

  /**
   * Adds another breadcrumb builder.
   *
   * @param \Drupal\consistent_breadcrumbs\ConsistentBreadcrumbBuilderInterface $builder
   *   The consistent breadcrumb builder to add.
   * @param int $priority
   *   Priority of the breadcrumb builder.
   */
  public function addBuilder(ConsistentBreadcrumbBuilderInterface $builder, int $priority);

}
