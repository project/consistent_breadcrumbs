<?php

declare(strict_types=1);

namespace Drupal\consistent_breadcrumbs;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

class ConsistentBreadcrumbManager implements ConsistentBreadcrumbManagerInterface {

  use StringTranslationTrait;

  private CacheBackendInterface $staticCache;

  private BreadcrumbBuilderInterface $breadcrumbManager;

  private ConsistentBreadcrumbsRoutingHelper $routingHelper;

  protected RequestStack $requestStack;

  /**
   * The consistent breadcrumb builders, keyed by priority.
   *
   * @var \Drupal\consistent_breadcrumbs\ConsistentBreadcrumbBuilderInterface[][]
   */
  private array $builders = [];

  /**
   * The sorted consistent breadcrumb builders.
   *
   * @var \Drupal\consistent_breadcrumbs\ConsistentBreadcrumbBuilderInterface[]
   */
  private ?array $sortedBuilders;

  public function __construct(CacheBackendInterface $staticCache, BreadcrumbBuilderInterface $breadcrumbManager, ConsistentBreadcrumbsRoutingHelper $routingHelper, RequestStack $requestStack) {
    $this->staticCache = $staticCache;
    $this->breadcrumbManager = $breadcrumbManager;
    $this->routingHelper = $routingHelper;
    $this->requestStack = $requestStack;
  }

  public function addBuilder(ConsistentBreadcrumbBuilderInterface $builder, int $priority): void {
    $this->builders[$priority][] = $builder;
    // Force the builders to be re-sorted.
    $this->sortedBuilders = NULL;
  }

  /**
   * Returns the sorted array of breadcrumb builders.
   *
   * @return \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface[]
   *   An array of breadcrumb builder objects.
   */
  protected function getSortedBuilders(): array {
    if (!isset($this->sortedBuilders)) {
      // Sort the builders according to priority.
      krsort($this->builders);
      // Merge nested builders from $this->builders into $this->sortedBuilders.
      $this->sortedBuilders = [];
      foreach ($this->builders as $builders) {
        $this->sortedBuilders = array_merge($this->sortedBuilders, $builders);
      }
    }
    return $this->sortedBuilders;
  }

  /**
   * @inheritDoc
   */
  public function applies(RouteMatchInterface $route_match): bool {
    $breadcrumb = $this->cachedMaybeBuild($route_match);
    return boolval($breadcrumb->getLinks());
  }

  /**
   * @inheritDoc
   */
  public function build(RouteMatchInterface $routeMatch): Breadcrumb {
    $breadcrumb = $this->cachedMaybeBuild($routeMatch);
    assert($breadcrumb);
    return $breadcrumb;
  }

  /**
   * Build the breadcrumb, cached.
   *
   * To compute ::applies(), we have to do all the work, wo we spare re-doing
   * everything on ::build().
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *
   * @return \Drupal\Core\Breadcrumb\Breadcrumb
   */
  private function cachedMaybeBuild(RouteMatchInterface $routeMatch): Breadcrumb {
    // Breadcrumb may not only depend on routeMatch, but also on query args.
    $cid = serialize([
      $routeMatch->getRouteName(),
      $routeMatch->getRawParameters(),
      strval($this->requestStack->getCurrentRequest()),
    ]);
    if ($hit = $this->staticCache->get($cid)) {
      $breadcrumb = $hit->data;
    }
    else {
      $breadcrumb = $this->maybeBuild($routeMatch);
      $this->staticCache->set($cid, $breadcrumb);
    }
    return $breadcrumb;
  }

  /**
   * Build the breadcrumbs if possible.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *
   * @return \Drupal\Core\Breadcrumb\Breadcrumb
   *   The breadcrumb object contains no links if no match.
   */
  private function maybeBuild(RouteMatchInterface $routeMatch): Breadcrumb {
    // dump([__FUNCTION__ . ':call' => Url::fromRouteMatch($routeMatch)->toString()]);
    // Collect cacheability of non-matches and non-access.
    // Technically, not all breadcrumbs need the route context, but in practice
    // it's convenient to default it.
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheContexts(['route']);
    foreach ($this->getSortedBuilders() as $builder) {
      // dump([__FUNCTION__ . ':try' => get_class($builder)]);
      $isFinalItem = NULL;
      $currentItem = $builder->getBreadcrumbItem($routeMatch, $isFinalItem);
      // Collect cacheability, with or without link.
      $cacheability->addCacheableDependency($currentItem);
      if (
        !$currentItem->getCacheContexts()
        && !$currentItem->getCacheTags()
      ) {
        throw new \LogicException(sprintf('Builder returned result without cacheability: %s', get_class($builder)));
      }
      $link = $currentItem->getLink();
      if (!$link) {
        // dump(__FUNCTION__ . ':noLink');
        continue;
      }
      $linkRouteMatch = $this->routingHelper->routeMatchFromUrl($link->getUrl(), $accessCacheability, $routeTitle);
      // Do not pursue this breadcrumb item if the link does not
      // have access. Also, no need to collect cacheability further, as the
      // final result does not depend on that. Nevertheless, keep
      // cacheability of this non-access, as any breadcrumb trail from a
      // later builder depends on the earlier failing access checks.
      $cacheability->addCacheableDependency($accessCacheability);
      if (!$linkRouteMatch) {
        // dump(__FUNCTION__ . ':noRouteMatch');
        continue;
      }

      $isFront = $this->routingHelper->routeMatchIsFront($linkRouteMatch);

      // Set link text to route title if empty.
      // Let frontpage have a consistent name regardless of route.
      if (!$link->getText()) {
        if ($isFront) {
          $link->setText(t('Frontpage'));
        }
        else {
          $link->setText($routeTitle);
        }
      }

      // If builder did not state otherwise, front link is final link.
      if (is_null($isFinalItem)) {
        $isFinalItem = $isFront;
      }

      if ($isFinalItem) {
        // dump(__FUNCTION__ . ':finalItem');
        // Done! We found a breadcrumb trail from currentRoute to Front.
        $breadcrumbs = new Breadcrumb();
      }
      else {
        // dump(__FUNCTION__ . ':recursion');
        // Try ConsistentBreadcrumbs recursion.
        $maybeBeforeItemBreadcrumbs = $this->maybeBuild($linkRouteMatch);
        $cacheability->addCacheableDependency($maybeBeforeItemBreadcrumbs);
        if ($maybeBeforeItemBreadcrumbs->getLinks()) {
          $breadcrumbs = $maybeBeforeItemBreadcrumbs;
        }
      }

      // We found a link item and the breadcrumbs preceding it, so done.
      if (isset($breadcrumbs)) {
        $result = $this->appendLink($breadcrumbs, $link);
        // Current item cacheability is already included.
        $result->addCacheableDependency($cacheability);
        // dump([__FUNCTION__ . ':success' => get_defined_vars()]);
        return $result;
      }
    }
    // No match at all.
    $result = new Breadcrumb();
    $result->addCacheableDependency($cacheability);
    // dump(__FUNCTION__ . ':fail');
    return $result;
  }

  private function appendLink(Breadcrumb $breadcrumbs, Link $link): Breadcrumb {
    $result = new Breadcrumb();
    $links = $breadcrumbs->getLinks();
    $lastlink = end($links);
    // Some core breadcrumb builders add the current page to the end. Omit this
    // duplication, and keep the ConsistentBreadcrumb link.
    // @see \Drupal\system\PathBasedBreadcrumbBuilder::build
    if ($lastlink && $link->getUrl()->toString() === $lastlink->getUrl()->toString()) {
      array_pop($links);
    }
    $result->setLinks($links)
      ->addLink($link)
      ->addCacheableDependency($breadcrumbs);
    return $result;
  }

}
