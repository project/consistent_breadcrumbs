<?php

namespace Drupal\consistent_breadcrumbs;

use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Drupal\Core\Link;

/**
 * A Consistent breadcrumb item is a link with cacheability.
 */
final class BreadcrumbItem {

  use RefinableCacheableDependencyTrait;

  protected ?Link $link;

  private function __construct() {}

  public static function create(?Link $link = NULL): BreadcrumbItem {
    $instance = new self();
    $instance->link = $link;
    return $instance;
  }

  public function getLink(): ?Link {
    return $this->link;
  }

  public function setLink(?Link $link): self {
    $this->link = $link;
    return $this;
  }

}
