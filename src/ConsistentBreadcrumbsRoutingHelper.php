<?php

declare(strict_types=1);

namespace Drupal\consistent_breadcrumbs;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Routing\AccessAwareRouterInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

class ConsistentBreadcrumbsRoutingHelper {

  private RequestMatcherInterface $requestMatcher;

  private TitleResolverInterface $titleResolver;

  private ConfigFactoryInterface $configFactory;

  private CurrentPathStack $currentPath;

  private InboundPathProcessorInterface $inboundPathProcessor;

  private AccessManagerInterface $accessManager;

  /**
   * Static cache for ::routeMatchIsFront
   */
  private \SplObjectStorage $routeMatchIsFront;

  /**
   * Static cache for frontPagePath().
   *
   * @var string;
   */
  private string $frontPagePath;

  public function __construct(RequestMatcherInterface $requestMatcher, TitleResolverInterface $titleResolver, ConfigFactoryInterface $configFactory, CurrentPathStack $currentPath, InboundPathProcessorInterface $inboundPathProcessor, AccessManagerInterface $accessManager) {
    $this->requestMatcher = $requestMatcher;
    $this->titleResolver = $titleResolver;
    $this->configFactory = $configFactory;
    $this->currentPath = $currentPath;
    $this->inboundPathProcessor = $inboundPathProcessor;
    $this->accessManager = $accessManager;

    $this->routeMatchIsFront = new \SplObjectStorage();
  }


  /**
   * Create a RouteMatch from an Url object.
   *
   * @param \Drupal\Core\Url $url
   *   The URL.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $accessCacheability
   *   The access cacheability returned.
   * @param null $routeTitle
   *   The route title, returned by reference.
   *
   * @return \Drupal\Core\Routing\RouteMatchInterface|null
   *   The route match, or NULL if no route or current user has no access.
   */
  public function routeMatchFromUrl(Url $url, RefinableCacheableDependencyInterface &$accessCacheability = NULL, &$routeTitle = NULL) {
    $accessCacheability = new CacheableMetadata();
    // @see \Drupal\system\PathBasedBreadcrumbBuilder::getRequestForPath
    $path = $url->toString();
    $request = Request::create($path);
    $request->headers->set('Accept', 'text/html');
    $processed = $this->inboundPathProcessor->processInbound($path, $request);
    $this->currentPath->setPath($processed, $request);
    // Attempt to match this path to provide a fully built request.
    // dump([__FUNCTION__ => get_defined_vars()]);
    try {
      // We can't use RouteMatch::createFromRequest without first having the
      // router populate the request attributes.
      $attributes = $this->requestMatcher->matchRequest($request);
      $request->attributes->add($attributes);
    }
    catch (ParamNotConvertedException $e) {
      // dump('ParamNotConvertedException');
      return NULL;
    }
    catch (ResourceNotFoundException $e) {
      // dump('ResourceNotFoundException');
      return NULL;
    }
    catch (MethodNotAllowedException $e) {
      // dump('MethodNotAllowedException');
      return NULL;
    }
    catch (AccessDeniedHttpException $e) {
      // dump('AccessDeniedHttpException');
      return NULL;
    }

    // Add access cacheability.
    // @see \Drupal\Core\Routing\AccessAwareRouter::checkAccess
    assert($request->attributes->has(AccessAwareRouterInterface::ACCESS_RESULT));

    $routeMatch = RouteMatch::createFromRequest($request);
    $accessResult = $this->accessManager->check($routeMatch, NULL, NULL, TRUE);
    $accessCacheability->addCacheableDependency($accessResult);

    // Retrieve route title.
    $routeTitle = $this->titleResolver->getTitle($request, $routeMatch->getRouteObject());
    if (!$routeTitle) {
      // Fallback to using the raw path component as the title if the
      // route is missing a _title or _title_callback attribute.
      $pathElements = explode('/', $url->getInternalPath());
      $routeTitle = str_replace(['-', '_'], ' ', Unicode::ucfirst(end($pathElements)));
    }
    // dump([__FUNCTION__ => get_defined_vars()]);
    return $routeMatch;
  }

  /**
   * Check if a route match is identical to the front page.
   *
   * Checking this in not trivial as the front page may have aliases.
   *
   * @param \Drupal\Core\Url $url
   *   The url.
   *
   * @return bool
   *   True if the url is identical to the front page.
   *
   * @see \Drupal\Core\Path\PathMatcher::isFrontPage
   */
  public function routeMatchIsFront(RouteMatchInterface $routeMatch) {
    if (!isset($this->routeMatchIsFront[$routeMatch])) {
      $matchedUrl = Url::fromRouteMatch($routeMatch);
      $this->routeMatchIsFront[$routeMatch] = $matchedUrl->getRouteName()
        && '/' . $matchedUrl->getInternalPath() === $this->getFrontPagePath();
      // dump([__FUNCTION__ => [$matchedUrl->toString() => $this->routeMatchIsFront[$routeMatch]]]);
    }
    return $this->routeMatchIsFront[$routeMatch];
  }

  /**
   * Gets the current front page path.
   *
   * @return string
   *   The front page path.
   */
  protected function getFrontPagePath() {
    // Lazy-load front page config.
    if (!isset($this->frontPagePath)) {
      $this->frontPagePath = $this->configFactory->get('system.site')
        ->get('page.front');
    }
    return $this->frontPagePath;
  }

}
